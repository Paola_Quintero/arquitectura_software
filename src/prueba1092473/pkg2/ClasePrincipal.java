/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba1092473.pkg2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author SergioIván
 */
public class ClasePrincipal {
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Se crean objetos de tipo FiguraGeometrica, con el constructor de Circulo
        FiguraGeometrica cir1=new Circulo(3.5f);  
        FiguraGeometrica cir2=new Circulo(4.2f);
        //Tambien debe funcionar segun su codigo las lineas que estan comentariadas...
        //Pruebe quitar los comentarios y ejecutar
       // FiguraGeometrica cua2=new Cuadrado(7.3f);
        //FiguraGeometrica trian1=new Triangulo(4.1f);
        //Agregar mas objetos
        //Se va a crear una lista de tipo ArrayList...Recuerde las colecciones
        
        List <FiguraGeometrica> serieDeFiguras=new ArrayList <FiguraGeometrica>(); 
        serieDeFiguras.add(cir2);
        serieDeFiguras.add(cir1);
     
        float areatotal=0;
        
        for (FiguraGeometrica serieDeFigura : serieDeFiguras) {
            
            areatotal=areatotal+serieDeFigura.calcularArea();
        
        }
        System.out.println("Para un total de: "+serieDeFiguras.size());
            System.out.println("El área total de las figuras es: "+areatotal);
            
            
        // TODO code application logic here
    }
    
}
